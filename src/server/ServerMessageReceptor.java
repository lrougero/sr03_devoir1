package server;

import java.io.DataInputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class ServerMessageReceptor extends Thread{
    public static List<User> list_users;
    private User client;

    static {
        list_users = new ArrayList<>();
    }

    public ServerMessageReceptor(User client){
        list_users.add(client);
        this.client = client;
    }

    @Override
    public void run(){
        try {
            DataInputStream in = new DataInputStream(this.client.sock.getInputStream());

            while (true){
                // reception du message
            	try {
                String message = in.readUTF();
                if (message.equals("exit")) {
                    list_users.remove(this.client);
                    for (User dest : list_users){
                        dest.out.writeUTF(this.client.username+" a quitté la conversation.");
                    }
                    break;
                }

                // envoie a� tous les clients meme l'envoyeur
                for (User dest : list_users){
                    try{
                        dest.out.writeUTF(this.client.username+" a dit : "+message);
                    } catch (IOException e){
                        e.printStackTrace();
                        System.out.println("erreur envoi user : "+dest.username);
                    }
                }
            }
            //a mettre dans une exception
        }catch(IOException ex) {
            	System.out.println("connexion terminee avec user : "+ this.client.username);
            	this.interrupt();
            	ex.printStackTrace();
        }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public User getClient() {
        return client;
    }
}
