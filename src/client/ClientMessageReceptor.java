package client;

import server.User;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;

public class ClientMessageReceptor extends Thread{
    private final User client;
    private final DataInputStream in;
    private final DataOutputStream out;

    public ClientMessageReceptor(User client, DataInputStream in, DataOutputStream out) {
        this.client = client;
        this.in = in;
        this.out = out;
    }

    @Override
    public void run(){
        // handle receiving message from own user : strip "xxx a dit :" to message printed
        try {
            while (true){
                String message = this.in.readUTF();

                if (message.equals(this.client.username+" a quitté le conservation.")){
                    //client.getSock().close();
                    break;
                }

                if (message.contains(this.client.username+" a dit :")){
                    //strip first part and print with indent
                    message=message.split("a dit :")[1];
                }

                //affichage du message
                System.out.println(message);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
