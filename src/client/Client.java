package client;

import server.User;

import javax.annotation.processing.SupportedSourceVersion;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.net.Socket;
import java.util.Scanner;

public class Client {
    private User user ;

    public void setUser(User user) {
        this.user = user;
    }

    public void launch(int port) throws IOException {
        Socket client=new Socket("localhost", port);
        DataOutputStream out = new DataOutputStream(client.getOutputStream());
        DataInputStream in = new DataInputStream(client.getInputStream());

        try{
            System.out.println("Entrez votre pseudo :");
            Scanner sc = new Scanner(System.in);
            String resp_username = sc.next();
            out.writeUTF(resp_username);

            String resp = in.readUTF();
            while (resp.equals("Username not available")){
                System.out.println("Pseudo non disponible...");
                System.out.println("Entrez un autre pseudo : ");
                Scanner sc2 = new Scanner(System.in);
                resp_username = sc2.nextLine();
                out.writeUTF(resp_username);
                resp = in.readUTF();
            }

            this.setUser(new User(resp_username,client,out));

            String first = in.readUTF(); //
            System.out.println(first); // message : xxx a rejoint la conversation.
            System.out.println("_________________");
        } catch(IOException e){
            e.printStackTrace();
        }

        ClientMessageReceptor user_thread = new ClientMessageReceptor(user,in,out);
        user_thread.start();

        // boucle d'ecriture
        String to_send;
        while (true) {
            Scanner sc = new Scanner(System.in);
            to_send = sc.nextLine();

            if (to_send.equals("exit")){
                sc.close();
                System.out.println("Disconnected");
                break;
            }else {
                out.writeUTF(to_send);
            }
        }

    }
}
